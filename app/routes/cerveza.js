var routerCerveza = require('express').Router()

  routerCerveza.get('/search', function(req, res) {
    res.json({ message: 'Vas a buscar una cerveza' })
  })

  routerCerveza.get('/', function(req, res) {
    res.json({ message: 'Estás conectado a la API. Recurso: cervezas' })
  })

  routerCerveza.get('/:id', function(req, res) {
    res.json({ message: 'Vas a obtener la cerveza con id ' + req.params.id })
  })

  routerCerveza.post('/', function(req, res) {
    res.json({ message: 'Vas a añadir una cerveza' })
  })

  module.exports = routerCerveza