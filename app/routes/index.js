var router = require('express').Router()
var cervezas = require('./cerveza')

router.use('/cervezas', cervezas)

router.get('/', function (req, res) {
    res.status(200).json({ message: 'Estás conectado a nuestra API en Heroku' })
})
  
module.exports = router