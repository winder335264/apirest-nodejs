var express = require('express') //llamamos a Express
var app = express()               
var bodyParser = require('body-parser')        
var router = require('./routes')


var port = process.env.PORT || 3000  // establecemos nuestro puerto

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())   

app.use('/', router)

app.listen(port)
console.log('API alojada en heroku escuchando en el puerto ' + port)